# Simple Temporal Networks

Jupyter walkthrough of the 16.412 Lecture on Programs with Flexible Time.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mit-mers%2Fjupyter%2Fsimple-temporal-networks/main)

## Getting started

The `Dockerfile` in this repo is there to make sure the container that runs the Binder notebook has `graphviz` installed. You can run the notebook locally in the same environment with:

```sh
docker build -t lesson .
docker run -it --rm -p 8888:8888 lesson jupyter-lab --ip=0.0.0.0 --port=8888
```
