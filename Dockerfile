# Sets up the container for our notebook. This Dockerfile is used by Binder

# See https://mybinder.readthedocs.io/en/latest/tutorials/dockerfile.html

FROM jupyter/base-notebook:notebook-6.4.8

ARG NB_USER=jovyan
ARG NB_UID=1000
ENV USER ${NB_USER}
ENV NB_UID ${NB_UID}
ENV HOME /home/${NB_USER}

USER root

RUN apt-get update -y && apt-get install -y graphviz

# Make sure the contents of our repo are in ${HOME}
COPY . ${HOME}
RUN chown -R ${NB_UID} ${HOME}
USER ${NB_USER}

RUN pip install -r requirements.txt
