{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Modeling Uncertainty in Observations\n",
    "\n",
    "Have you ever waited on someone to call you to let you know something happened? Or have you wondered how future Martian explorers will be able to reason about timelines when it takes minutes for messages from Mission Control to reach them? Simple Temporal Networks with Uncertainty give us the tools for modeling ambiguity, but they still assume an instantaneous relationship between when events occur and when the agent observes them. What if we remove that assumption? What if the agent doesn't learn about events until much later? Or maybe never? In that case, we need a new kind of controllability - _Delay Controllability_.\n",
    "\n",
    "In this lesson, we will explore how to model STNUs when there are delays in communication.\n",
    "\n",
    "Run the cell below to set up the notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The autoreload extension is already loaded. To reload it, use:\n",
      "  %reload_ext autoreload\n"
     ]
    }
   ],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2\n",
    "import networkx as nx\n",
    "import numpy as np\n",
    "from render import *\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## An Intuition for Observation Delay\n",
    "\n",
    "Let's look at a scenario where observation delay factors into our decision making.\n",
    "\n",
    "> Picture yourself doing chores around the house. You have a fancy new clothes dryer that senses how dry your clothes are and will keep spinning until they're completely dry. When it's done, it will beep until you open the door because it doesn't want your clothes to get wrinkled. It takes a minute for you to throw a load of wet laundry into the dryer and then head back to the kitchen to keep cleaning. You know the machine will take between 40 and 60 minutes to dry your clothes. You're listening to music with your headphones on, so you may not immediately hear when the wash is done. You want to make sure that your dry clothes haven't been sitting for more than 30 minutes before you fold them, but you want to wait at least 10 minutes to let them cool off a little first.\n",
    "\n",
    "We can naively write the following STNU, letting $L$ represent loading the dryer, $D$ represent the dryer cycle, $K$ represent cleaning the kitchen, and $F$ represent folding."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/svg+xml": "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"650pt\" height=\"135pt\" viewBox=\"0.00 0.00 520.00 108.00\">\n<g id=\"graph0\" class=\"graph\" transform=\"scale(1.25 1.25) rotate(0) translate(4 104)\">\n<title>G</title>\n<polygon fill=\"white\" stroke=\"transparent\" points=\"-4,4 -4,-104 516,-104 516,4 -4,4\"/>\n<!-- L -->\n<g id=\"node1\" class=\"node\">\n<title>L</title>\n<ellipse fill=\"none\" stroke=\"black\" cx=\"21.5\" cy=\"-53.5\" rx=\"21.5\" ry=\"21.5\"/>\n<text text-anchor=\"middle\" x=\"21.5\" y=\"-49.8\" font-family=\"Times,serif\" font-size=\"14.00\">L</text>\n</g>\n<!-- D&#45; -->\n<g id=\"node2\" class=\"node\">\n<title>D-</title>\n<ellipse fill=\"none\" stroke=\"black\" cx=\"131.5\" cy=\"-53.5\" rx=\"21.5\" ry=\"21.5\"/>\n<text text-anchor=\"middle\" x=\"131.5\" y=\"-49.8\" font-family=\"Times,serif\" font-size=\"14.00\">D-</text>\n</g>\n<!-- L&#45;&gt;D&#45; -->\n<g id=\"edge1\" class=\"edge\">\n<title>L-&gt;D-</title>\n<path fill=\"none\" stroke=\"black\" d=\"M43.07,-53.5C59.03,-53.5 81.42,-53.5 99.54,-53.5\"/>\n<polygon fill=\"black\" stroke=\"black\" points=\"99.64,-57 109.64,-53.5 99.64,-50 99.64,-57\"/>\n<text text-anchor=\"middle\" x=\"76.5\" y=\"-57.3\" font-family=\"Times,serif\" font-size=\"14.00\">[1, 1]</text>\n</g>\n<!-- D+ -->\n<g id=\"node3\" class=\"node\">\n<title>D+</title>\n<ellipse fill=\"none\" stroke=\"black\" stroke-dasharray=\"5,2\" cx=\"315.5\" cy=\"-78.5\" rx=\"21.5\" ry=\"21.5\"/>\n<text text-anchor=\"middle\" x=\"315.5\" y=\"-74.8\" font-family=\"Times,serif\" font-size=\"14.00\">D+</text>\n</g>\n<!-- D&#45;&#45;&gt;D+ -->\n<g id=\"edge2\" class=\"edge\">\n<title>D--&gt;D+</title>\n<path fill=\"none\" stroke=\"black\" stroke-dasharray=\"5,2\" d=\"M153.02,-56.32C184.92,-60.7 246.59,-69.17 283.8,-74.28\"/>\n<polygon fill=\"black\" stroke=\"black\" points=\"283.67,-77.8 294.05,-75.69 284.62,-70.86 283.67,-77.8\"/>\n<text text-anchor=\"middle\" x=\"193\" y=\"-67.3\" font-family=\"Times,serif\" font-size=\"14.00\">[40, 60]</text>\n</g>\n<!-- K&#45; -->\n<g id=\"node4\" class=\"node\">\n<title>K-</title>\n<ellipse fill=\"none\" stroke=\"black\" cx=\"254.5\" cy=\"-27.5\" rx=\"21.5\" ry=\"21.5\"/>\n<text text-anchor=\"middle\" x=\"254.5\" y=\"-23.8\" font-family=\"Times,serif\" font-size=\"14.00\">K-</text>\n</g>\n<!-- D&#45;&#45;&gt;K&#45; -->\n<g id=\"edge3\" class=\"edge\">\n<title>D--&gt;K-</title>\n<path fill=\"none\" stroke=\"black\" d=\"M152.34,-46.76C158.29,-44.89 164.87,-42.97 171,-41.5 187.99,-37.43 207.22,-34.09 222.84,-31.7\"/>\n<polygon fill=\"black\" stroke=\"black\" points=\"223.62,-35.12 233,-30.2 222.6,-28.2 223.62,-35.12\"/>\n<text text-anchor=\"middle\" x=\"193\" y=\"-45.3\" font-family=\"Times,serif\" font-size=\"14.00\">[0, ∞]</text>\n</g>\n<!-- F -->\n<g id=\"node5\" class=\"node\">\n<title>F</title>\n<ellipse fill=\"none\" stroke=\"black\" cx=\"490.5\" cy=\"-50.5\" rx=\"21.5\" ry=\"21.5\"/>\n<text text-anchor=\"middle\" x=\"490.5\" y=\"-46.8\" font-family=\"Times,serif\" font-size=\"14.00\">F</text>\n</g>\n<!-- D+&#45;&gt;F -->\n<g id=\"edge4\" class=\"edge\">\n<title>D+-&gt;F</title>\n<path fill=\"none\" stroke=\"black\" d=\"M337.1,-76.17C363.57,-73.04 410.9,-67 451,-59.5 453.85,-58.97 456.8,-58.37 459.75,-57.73\"/>\n<polygon fill=\"black\" stroke=\"black\" points=\"460.61,-61.13 469.59,-55.51 459.06,-54.3 460.61,-61.13\"/>\n<text text-anchor=\"middle\" x=\"377\" y=\"-77.3\" font-family=\"Times,serif\" font-size=\"14.00\">[10, 30]</text>\n</g>\n<!-- K+ -->\n<g id=\"node6\" class=\"node\">\n<title>K+</title>\n<ellipse fill=\"none\" stroke=\"black\" cx=\"377\" cy=\"-21.5\" rx=\"21.5\" ry=\"21.5\"/>\n<text text-anchor=\"middle\" x=\"377\" y=\"-17.8\" font-family=\"Times,serif\" font-size=\"14.00\">K+</text>\n</g>\n<!-- K&#45;&#45;&gt;K+ -->\n<g id=\"edge5\" class=\"edge\">\n<title>K--&gt;K+</title>\n<path fill=\"none\" stroke=\"black\" d=\"M276.21,-26.47C295.18,-25.52 323.51,-24.11 345.14,-23.04\"/>\n<polygon fill=\"black\" stroke=\"black\" points=\"345.34,-26.53 355.15,-22.54 344.99,-19.54 345.34,-26.53\"/>\n<text text-anchor=\"middle\" x=\"315.5\" y=\"-30.3\" font-family=\"Times,serif\" font-size=\"14.00\">[0, ∞]</text>\n</g>\n<!-- K+&#45;&gt;F -->\n<g id=\"edge6\" class=\"edge\">\n<title>K+-&gt;F</title>\n<path fill=\"none\" stroke=\"black\" d=\"M397.93,-26.68C415.03,-31.13 439.92,-37.61 459.44,-42.68\"/>\n<polygon fill=\"black\" stroke=\"black\" points=\"458.8,-46.13 469.36,-45.26 460.57,-39.36 458.8,-46.13\"/>\n<text text-anchor=\"middle\" x=\"434\" y=\"-44.3\" font-family=\"Times,serif\" font-size=\"14.00\">[0, ∞]</text>\n</g>\n</g>\n</svg>",
      "text/plain": [
       "<IPython.core.display.SVG object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "stn = nx.DiGraph()\n",
    "stn.add_edge('L', 'D-', stc=[1, 1])\n",
    "stn.add_edge('D-', 'D+', ctg=[40, 60])\n",
    "stn.add_edge('D-', 'K-', stc=[0, np.inf])\n",
    "stn.add_edge('K-', 'K+', stc=[0, np.inf])\n",
    "stn.add_edge('K+', 'F', stc=[0, np.inf])\n",
    "stn.add_edge('D+', 'F', stc=[10, 30])\n",
    "display_stn(stn)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's look at a scenario where observation delay factors into our decision making.\n",
    "\n",
    "> At 8am every morning, Sam brews a large pot of coffee for the office. The machine Sam uses is old and temperamental, so it can take anywhere between 15 and 30 minutes before the coffee is ready. The coffee machine is several rooms away from the office, meaning workers in the office have no immediate way of knowing when the coffee is ready. Every morning, Sam sends an email to the group to let them know the coffee is ready, but only after he finishes his first cup. Drinking a cup of coffee takes Sam somewhere between 5 and 15 minutes. Alex waits in the office and wants to get coffee after it has cooled a bit; she finds the coffee temperature to be optimal between 20 and 30 minutes after the coffee is ready. It takes Alex 5 minutes to finish her coffee, and once she is done, she goes to a client meeting that starts at 9am.\n",
    "\n",
    "We can naively build the following STNU, letting $A$ represent when Sam starts making coffee, $B$ represent when the coffee finishes brewing, $C$ represent Alex getting coffee, and $D$ represent when Alex gets to the meeting."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/svg+xml": "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"509pt\" height=\"106pt\" viewBox=\"0.00 0.00 407.00 85.00\">\n<g id=\"graph0\" class=\"graph\" transform=\"scale(1.25 1.25) rotate(0) translate(4 81)\">\n<title>G</title>\n<polygon fill=\"white\" stroke=\"transparent\" points=\"-4,4 -4,-81 403,-81 403,4 -4,4\"/>\n<!-- A -->\n<g id=\"node1\" class=\"node\">\n<title>A</title>\n<ellipse fill=\"none\" stroke=\"black\" cx=\"21.5\" cy=\"-21.5\" rx=\"21.5\" ry=\"21.5\"/>\n<text text-anchor=\"middle\" x=\"21.5\" y=\"-17.8\" font-family=\"Times,serif\" font-size=\"14.00\">A</text>\n</g>\n<!-- B -->\n<g id=\"node2\" class=\"node\">\n<title>B</title>\n<ellipse fill=\"none\" stroke=\"black\" stroke-dasharray=\"5,2\" cx=\"144.5\" cy=\"-55.5\" rx=\"21.5\" ry=\"21.5\"/>\n<text text-anchor=\"middle\" x=\"144.5\" y=\"-51.8\" font-family=\"Times,serif\" font-size=\"14.00\">B</text>\n</g>\n<!-- A&#45;&gt;B -->\n<g id=\"edge1\" class=\"edge\">\n<title>A-&gt;B</title>\n<path fill=\"none\" stroke=\"black\" stroke-dasharray=\"5,2\" d=\"M42.48,-27.11C61.83,-32.55 91.43,-40.87 113.59,-47.1\"/>\n<polygon fill=\"black\" stroke=\"black\" points=\"112.92,-50.54 123.49,-49.88 114.81,-43.8 112.92,-50.54\"/>\n<text text-anchor=\"middle\" x=\"83\" y=\"-48.3\" font-family=\"Times,serif\" font-size=\"14.00\">[15, 30]</text>\n</g>\n<!-- D -->\n<g id=\"node3\" class=\"node\">\n<title>D</title>\n<ellipse fill=\"none\" stroke=\"black\" cx=\"377.5\" cy=\"-21.5\" rx=\"21.5\" ry=\"21.5\"/>\n<text text-anchor=\"middle\" x=\"377.5\" y=\"-17.8\" font-family=\"Times,serif\" font-size=\"14.00\">D</text>\n</g>\n<!-- A&#45;&gt;D -->\n<g id=\"edge2\" class=\"edge\">\n<title>A-&gt;D</title>\n<path fill=\"none\" stroke=\"black\" d=\"M43.06,-21.5C102.57,-21.5 275.22,-21.5 345.84,-21.5\"/>\n<polygon fill=\"black\" stroke=\"black\" points=\"345.88,-25 355.88,-21.5 345.88,-18 345.88,-25\"/>\n<text text-anchor=\"middle\" x=\"206\" y=\"-25.3\" font-family=\"Times,serif\" font-size=\"14.00\">[0, 60]</text>\n</g>\n<!-- C -->\n<g id=\"node4\" class=\"node\">\n<title>C</title>\n<ellipse fill=\"none\" stroke=\"black\" cx=\"267.5\" cy=\"-55.5\" rx=\"21.5\" ry=\"21.5\"/>\n<text text-anchor=\"middle\" x=\"267.5\" y=\"-51.8\" font-family=\"Times,serif\" font-size=\"14.00\">C</text>\n</g>\n<!-- B&#45;&gt;C -->\n<g id=\"edge3\" class=\"edge\">\n<title>B-&gt;C</title>\n<path fill=\"none\" stroke=\"black\" d=\"M166.03,-55.5C185.15,-55.5 213.91,-55.5 235.77,-55.5\"/>\n<polygon fill=\"black\" stroke=\"black\" points=\"235.87,-59 245.87,-55.5 235.87,-52 235.87,-59\"/>\n<text text-anchor=\"middle\" x=\"206\" y=\"-59.3\" font-family=\"Times,serif\" font-size=\"14.00\">[20, 30]</text>\n</g>\n<!-- C&#45;&gt;D -->\n<g id=\"edge4\" class=\"edge\">\n<title>C-&gt;D</title>\n<path fill=\"none\" stroke=\"black\" d=\"M288.31,-49.26C304.75,-44.09 328.38,-36.65 347.04,-30.78\"/>\n<polygon fill=\"black\" stroke=\"black\" points=\"348.33,-34.04 356.82,-27.7 346.23,-27.36 348.33,-34.04\"/>\n<text text-anchor=\"middle\" x=\"322.5\" y=\"-46.3\" font-family=\"Times,serif\" font-size=\"14.00\">[5, 5]</text>\n</g>\n</g>\n</svg>",
      "text/plain": [
       "<IPython.core.display.SVG object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "stn = nx.DiGraph()\n",
    "stn.add_edge('A', 'B', ctg=[15, 30])\n",
    "stn.add_edge('B', 'C', stc=[20, 30])\n",
    "stn.add_edge('C', 'D', stc=[5, 5])\n",
    "stn.add_edge('A', 'D', stc=[0, 60])\n",
    "display_stn(stn)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Something is missing. Remember, Sam does not send the email until he finishes his coffee, which takes between 5 and 10 minutes. In effect, while $B$ may occur in $[15, 30]$, we won't learn about $B$ for another $[5, 15]$ after it finishes. But Alex needs to factor this time into deciding when to get coffee, $C$.\n",
    "\n",
    "Let's add a new decorator to the STNU to indicate the presence of observation delay on contingent events. We'll call it a delay function, $\\gamma(e_c)$, where $e_c$ is a contingent event. For this implementation, we'll let $\\gamma(e_c)$ return set bounded range, $[l, u]$, just like the other constraints in an STNU. In effect, $\\gamma(e_c) = [l, u]$ indicates that we will not learn that $e_c$ has resolved for an additional $[l, u]$ units of time.\n",
    "\n",
    "(Unfortunately, we're at the limit of the kind of graphs that we can easily generate with the `graphviz` package. We'll be inserting hand-drawn STNUs from here on out.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# insert img of STNU with gamma"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is important to note that observations of contingent events with $\\gamma$ functions provide less information than observations of other events. With uncertainty in both the contingent event and our delayed observation of it, we lose the guarantee that we know for certain when an event actually occured.\n",
    "\n",
    "Consider event $B$, Alex learning the coffee is brewed, in the STNU above. There is a range of outcomes that could lead to an observation of $B$ at 25. For instance, the coffee could have finished brewing in 15 minutes and Sam could have waited 10 minutes to email Alex. Or the coffee could have finished in 20 minutes and Sam could have waited 5 minutes to email Alex. In fact, any combination of $B + \\gamma(B) = 25$ where $B \\in [15, 30], \\gamma(B) \\in [5, 15]$ is valid. By our formulation, we _do not know_ which combination led to our observation at 25."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Thus, communication in this scenario does not offer certainty about when to act.\n",
    "\n",
    "Given that Sam starts brewing at 8am, if Alex receives the email any time before 8:40am, it is simple for her to devise an execution policy based on the fact that the email arrives between 5 and 15 minutes after the coffee is brewed. If she waits 15 minutes after receiving the email, she is guaranteed to get her coffee between 20 and 30 minutes after it is brewed, at which time the coffee will have cooled to her liking. The challenge for Alex comes when the email arrives after 8:40am. In this situation, waiting 15 minutes will make Alex late for her 9am meeting. To address the ambiguity around how Alex should act when the email arrives after 8:40am, we reason about the range of possible values associated with the time that it takes to brew coffee and the time for the email to be sent. Specifically, Alex knows that it takes at most 15 minutes for Sam to drink coffee and for the email to be sent. If an email has not arrived by 8:40am, the coffee must have taken at least 25 minutes to brew, finishing at 8:25am (8:25am + 15 minute delay = 8:40am). Coffee brewing also has an upper bound of 30 minutes, so it could have finished 10 minutes ago at 8:30am (8:30am + 10 minute delay = 8:40am). As a result, if Alex gets coffee at 8:55am, she has a guarantee that the coffee has been sitting out between 25 and 30 minutes (i.e., [8:55am - 8:30am, 8:55am - 8:25am] = [25, 30]). This is within Alex’s 20 to 30 minute window for waiting for the coffee to 4Uncertain Communication in Temporal Networks cool, while still giving her enough time drink her coffee and attend her meeting on time. Delay controllability, and more specifically, variable-delay controllability, formalizing checking for controllability with uncertain communications."
   ]
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "17f96b024b25748703f7a3e6c6f632adbaac076a7f04e19d9158e113b585037b"
  },
  "kernelspec": {
   "display_name": "Python 3.10.2 ('.venv': venv)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.2"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
