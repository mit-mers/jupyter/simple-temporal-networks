#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Code for visualizating STNs and distance graphs
"""

from IPython.display import display, SVG
import tempfile
import subprocess
import os
import numpy as np

def display_stn(g):
    display(SVG(graph_to_svg(g, writer_fn=write_stn_to_dot)))

def display_weighted_graph(g):
    display(SVG(graph_to_svg(g, writer_fn=write_weighted_graph_to_dot)))

def graph_to_svg(g, writer_fn):
    with tempfile.NamedTemporaryFile(mode='w', suffix=".dot", delete=False) as f:
        name = f.name
        writer_fn(g, f)
    # Call dot on the name
    svg_file = "{}.svg".format(name)
    result = subprocess.call(["dot", "-Tsvg", name, "-o", svg_file])
    with open(svg_file, "r") as f:
        svg = f.read()
    os.remove(name)
    os.remove(svg_file)
    return svg

def write_stn_to_dot(g, f):
    #dpi=90
    f.write("digraph G {\n  rankdir=LR;\ndpi=90\n")

    # track contingent events to draw them with dashed lines
    contingent_events = set()

    # Simple temporal constraints
    for (u, v) in g.edges():
        if (stc := g[u][v].get('stc')):
            f.write("  \"{}\" -> \"{}\" [label=\"{}\"];".format(u, v, format_stc(stc)))
        if (ctg := g[u][v].get('ctg')):
            contingent_events.add(v)
            f.write("  \"{}\" -> \"{}\" [label=\"{}\", style=dashed];".format(u, v, format_stc(ctg)))

    # any event that isn't contingent is requirement
    requirement_events = set(g.nodes()) - contingent_events

    # Nodes
    for v in list(requirement_events):
        f.write("  \"{}\" [shape=circle, width=0.6, fixedsize=false];\n".format(v))
    for v in list(contingent_events):
        f.write("  \"{}\" [shape=circle, width=0.6, fixedsize=false, style=dashed];\n".format(v))

    f.write("}")

def write_weighted_graph_to_dot(g, f):
    #dpi=90
    f.write("digraph G {\n  rankdir=LR;\ndpi=90\n")
    # Nodes
    for v in g.nodes():
        f.write("  \"{}\" [shape=circle, width=0.6, fixedsize=false];\n".format(v))
    # Simple temporal constraints
    for (u, v) in g.edges():
        w = g[u][v]['weight']
        f.write("  \"{}\" -> \"{}\" [label=\"{}\"];".format(u, v, format_num(w)))
    f.write("}")

def format_stc(stc):
    return "[{}, {}]".format(format_num(stc[0]), format_num(stc[1]))

def format_num(v):
    if v == np.inf:
        return "∞"
    elif v == -np.inf:
        return "-∞"
    else:
        return str(v)
